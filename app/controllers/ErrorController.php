<?php
namespace app\controllers;

class ErrorController extends BaseController{
    public function get404(){
        return $this->render('404.twig');
    }
    public function get500(){
        return $this->render('500.twig');
    }
    public function get415(){
        return $this->render('415.twig');
    }
}