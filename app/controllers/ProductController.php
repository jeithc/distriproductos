<?php
namespace app\controllers;
use app\controllers\BaseController;
use app\database\DatabaseHandler;

class ProductController extends BaseController{

    public function getIndex(){

        $sql="SELECT p.*, c.name as c_name  FROM products p
        inner join category c on c.id=p.id_category where p.active = 1 ";
        $products=DatabaseHandler::GetAll($sql);
        DatabaseHandler::Close();
        return $this->render('products.twig', ['menu'=>'product', 'products'=>$products, 'title'=>'producto']);
    }


    public function getEdit($id){
        $sql="SELECT p.*, c.id as c_name  FROM products p
        inner join category c on c.id=p.id_category
        where p.id = :id";
        $product=DatabaseHandler::GetRow($sql, array(':id'=>$id));

        $sql="SELECT * FROM  category where active = 1";
        $categories=DatabaseHandler::GetAll($sql);
        DatabaseHandler::Close();
        return $this->render('edit_products.twig', ['menu'=>'product', 'product'=>$product, 'categories'=>$categories, 'title'=>'editar producto']);
    }


    public function postEdit($id){
        $sql="UPDATE products SET
            `name` = :name,
            `description` = :description,
            bar_code = :bar_code,
            price = :price,
            available = :available,
            id_category = :id_category
            WHERE id =:id ";
        $data= array(':name'=> $_POST['nombre'],
        ':description'=> $_POST['desc'],
        ':bar_code'=> $_POST['bar'],
        ':price'=> $_POST['price'],
        ':id_category'=> $_POST['category'],
        ':available'=> $_POST['available'],
        ':id'=> $id);
        DatabaseHandler::Execute($sql,$data);
        DatabaseHandler::Close();
        header('Location:' . BASE_URL. 'product' );

    }

    public function getAdd(){
        $sql="SELECT * FROM  category where active = 1";
        $categories=DatabaseHandler::GetAll($sql);
        DatabaseHandler::Close();
        return $this->render('edit_products.twig', ['menu'=>'product', 'categories'=>$categories, 'title'=>'editar producto']);
    }


    public function postAdd(){
        $sql="INSERT INTO `products` (`name`,`description`,`bar_code`,`price`,`available`,`id_category`) 
          VALUES(:name,:description,:bar_code,:price,:available,:id_category)";
        $data= array(':name'=> $_POST['nombre'],
        ':description'=> $_POST['desc'],
        ':bar_code'=> $_POST['bar'],
        ':price'=> $_POST['price'],
        ':id_category'=> $_POST['category'],
        ':available'=> $_POST['available']);
        DatabaseHandler::Execute($sql,$data);
        DatabaseHandler::Close();
        header('Location:' . BASE_URL. 'product' );

    }

    public function getDelete($id){
        $sql="UPDATE products SET
            active = 0
            WHERE id =:id ";
        DatabaseHandler::Execute($sql,array(':id'=> $id));
        DatabaseHandler::Close();
        header('Location:' . BASE_URL. 'product' );

    }
    
}