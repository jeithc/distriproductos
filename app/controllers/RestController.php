<?php
namespace app\controllers;
use app\controllers\BaseController;
use app\database\DatabaseHandler;

class RestController extends BaseController{


    public function getIndex(){
        header("HTTP/1.1 200");
        $sql="SELECT * from category where active = 1";
        $categories=DatabaseHandler::GetAll($sql);
        DatabaseHandler::Close();
        header("HTTP/1.1 200");
        echo json_encode($categories);
    }


    public function getOrders(){
        try {
            $sql="SELECT * FROM `order` order by id desc";
            $orders=DatabaseHandler::GetAll($sql);
            DatabaseHandler::Close();
            header("HTTP/1.1 200");
            echo json_encode($orders);
        } catch (\Throwable $th) {
            header("HTTP/1.1 500");
            echo json_encode('{
                message: "error al consultar"
            }');
        }
    }


    public function getOrdersnum($id){
        try {
            $sql="SELECT p.*, c.name as c_name ,op.amount FROM products p
            inner join category c on c.id=p.id_category
            inner join order_products op on op.id_products = p.id 
            where op.id_order = :id";
            $products=DatabaseHandler::GetAll($sql, array(':id'=>$id));
            DatabaseHandler::Close();
            
            if ($products){
                header("HTTP/1.1 200");
                echo json_encode($products);

            }else{
                header("HTTP/1.1 201");
                echo json_encode('{
                    message: "no hay datos para mostrar"
                }');
            }
        } catch (\Throwable $th) {
            header("HTTP/1.1 500");
            echo json_encode('{
                message: "error al consultar"
            }');
        }
    }

    public function getCategory(){
        try {
            $sql="SELECT * from category";
            $categories=DatabaseHandler::GetAll($sql);
            DatabaseHandler::Close();
            header("HTTP/1.1 200");
            echo json_encode($categories);
        } catch (\Throwable $th) {
            header("HTTP/1.1 500");
            echo json_encode('{
                message: "error al consultar"
            }');
        }
    }


    public function getProducts(){
        try {
            $sql="SELECT * FROM `products`";
            $products=DatabaseHandler::GetAll($sql);
            DatabaseHandler::Close();
            header("HTTP/1.1 200");
            echo json_encode($products);
        } catch (\Throwable $th) {
            header("HTTP/1.1 500");
            echo json_encode('{
                message: "error al consultar"
            }');
        }
    }
    
}