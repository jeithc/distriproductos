<?php
namespace app\controllers;
use app\controllers\BaseController;
use app\database\DatabaseHandler;

class ListController extends BaseController{

    public function getIndex(){

        $sql="SELECT * FROM `order` order by id desc";
        $orders=DatabaseHandler::GetAll($sql);
        DatabaseHandler::Close();
        return $this->render('list_order.twig', ['menu'=>'orders', 'orders'=>$orders, 'title'=>'Ordenes']);
    }


    public function getNum($id){
        $sql="SELECT p.*, c.name as c_name ,op.amount FROM products p
        inner join category c on c.id=p.id_category
        inner join order_products op on op.id_products = p.id 
        where op.id_order = :id";
        $products=DatabaseHandler::GetAll($sql, array(':id'=>$id));
        DatabaseHandler::Close();
        return $this->render('data_order.twig', ['products'=> $products, 'menu'=>'orders', 'title'=>'Productos Orden '.$id]);
    }

    
}