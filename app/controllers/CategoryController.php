<?php
namespace app\controllers;
use app\controllers\BaseController;
use app\database\DatabaseHandler;

class CategoryController extends BaseController{

    public function getIndex(){
        $sql="SELECT * from category where active = 1";
        $categories=DatabaseHandler::GetAll($sql);
        DatabaseHandler::Close();
        return $this->render('categories.twig', ['menu'=>'category', 'categories'=>$categories, 'title'=>'Categoría']);
    }


    public function getEdit($id){
        $sql="SELECT * from category where id = :id";
        $category=DatabaseHandler::GetRow($sql, array(':id'=>$id));
        DatabaseHandler::Close();
        return $this->render('edit_category.twig', ['menu'=>'category', 'category'=>$category, 'categories'=>$category, 'title'=>'editar categoría']);
    }


    public function postEdit($id){
        $sql="UPDATE category SET
            `name` = :name,
            `description` = :description
            WHERE id =:id ";
        $data= array(':name'=> $_POST['nombre'],
        ':description'=> $_POST['desc'],
        ':id'=> $id);
        DatabaseHandler::Execute($sql,$data);
        DatabaseHandler::Close();
        header('Location:' . BASE_URL. 'category');
    }

    
    public function getAdd(){
        return $this->render('add_category.twig', ['menu'=>'category',  'title'=>'Agregar categoría']);
    }


    public function postAdd(){
        $sql="INSERT INTO `category` (`name`,`description`) 
          VALUES ( :name,:description)";
        $data= array(':name'=> $_POST['nombre'],
        ':description'=> $_POST['desc']);
        DatabaseHandler::Execute($sql,$data);
        DatabaseHandler::Close();
        header('Location:' . BASE_URL. 'category' );
    }


    public function getDelete($id){
        $sql="UPDATE category SET
            `active` = 0
            WHERE id =:id ";
        DatabaseHandler::Execute($sql,array(':id'=> $id));
        DatabaseHandler::Close();
        header('Location:' . BASE_URL. 'category' );
    }

    
}