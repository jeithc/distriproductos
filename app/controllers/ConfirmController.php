<?php
namespace app\controllers;
use app\controllers\BaseController;
use app\database\DatabaseHandler;
use app\log;
class ConfirmController extends BaseController{

    public function getIndex(){


        return $this->render('confirm.twig', ['menu'=>'listado', 'id'=>$_SESSION['id'], 'title'=>'Confirmación']);
    }

    
}