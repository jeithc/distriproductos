<?php
namespace app\controllers;
use Twig_Loader_Filesystem;
class BaseController{
    protected $templateEngine;
    protected $result;
    public function __construct(){
        $loader = new Twig_Loader_Filesystem('../views');
        $this->templateEngine = new \Twig_Environment($loader, [
            'debug'=> true,
            'cache'=> false
        ]);
        $this->templateEngine->addFilter(new \Twig_SimpleFilter('url', function($path){
            return BASE_URL . $path;
        }));
        $this->templateEngine->addFilter(new \Twig_SimpleFilter('estilo', function($path){
            return ESTILO . $path;
        }));
        $this->templateEngine->addFilter(new \Twig_SimpleFilter('documents', function($path){
            return BASE_URL."/".FILESPOLICY . $path;
        }));
        $this->templateEngine->addGlobal('session', $_SESSION);

        //$this->templateEngine->addExtension(new \Twig_Extension_Debug()); para poder imprimir arrays twig {{ dump(#array) }}
        $this->templateEngine->addExtension(new \Twig_Extensions_Extension_Intl());
    }
    public function render($fileName, $data = []){
        return $this->templateEngine->render($fileName, $data);
    }
}