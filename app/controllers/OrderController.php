<?php
namespace app\controllers;
use app\controllers\BaseController;
use app\database\DatabaseHandler;

class OrderController extends BaseController{
    public function getIndex(){
        $sql="SELECT p.*, c.name as c_name  FROM products p
        inner join category c on c.id=p.id_category 
        where available > 0 and p.active = 1";
        $products=DatabaseHandler::GetAll($sql);
        DatabaseHandler::Close();
        return $this->render('list_producs.twig', ['products'=> $products, 'menu'=>'list', 'title'=>'Listado Artículos']);
    }

    public function postIndex(){
        global $ahora;
        $total = [];
        $sql="INSERT INTO `order` (client_id_type,client_id,client_name,client_address,client_city,client_email,date) 
            VALUES(:client_id_type,:client_id,:client_name,:client_address,:client_city,:client_email, :date)";
            $data= array(':client_id_type'=> $_POST['id_type'],
                ':client_id' =>  $_POST['id_number'],
                ':client_name' =>  $_POST['name'],
                ':client_address' =>  $_POST['dir'],
                ':client_city'  =>  $_POST['city'],
                ':client_email'  =>  $_POST['email'],
                ':date'  =>  $ahora);
        DatabaseHandler::Execute($sql,$data);

        $sql="SELECT id  FROM `order`
        where client_id = :id order by id desc limit 1";
        $id=DatabaseHandler::GetOne($sql, array(':id'=>$_POST['id_number']));
        foreach($_POST['id'] as $clave => $valor){
            if ($valor){
                $total += [$_POST['prod'][$clave] => $valor];
                $sql="INSERT INTO order_products (id_products,id_order, amount) 
                    VALUES(:id_products,:id_order,:amount)";
                    $data= array(':id_products'=> $_POST['prod'][$clave],
                        ':id_order' =>  $id,
                        ':amount' =>  $valor);
                DatabaseHandler::Execute($sql,$data);
                $sql="UPDATE products SET available = available - :num where id = :id";
                $data= array(':num'=> $valor,
                    ':id' =>  $_POST['prod'][$clave]);
                DatabaseHandler::Execute($sql,$data);
            }
        }
        DatabaseHandler::Close();
        $_SESSION['id']=$id;
        header('Location:' . BASE_URL. 'confirm' );
    }

}