let home = document.location.href;
let id = home.lastIndexOf('/dashboard');
let url = home.substring(0, id)+"/events/dashboard";
let url2 = home.substring(0, id)+"/events/dashboardline";
let url3 = home.substring(0, id)+"/events/dashboardclient1";
let url4 = home.substring(0, id)+"/events/dashboardclient2";

if($("#doughnut1").length == 0) {
    $.ajax({
        url: url,
        method: 'GET',
        data: {
            prueba: 'prueba'
        },
        success: function(response){
    
        pintar(response)
        },
        error: function(error){
            console.log(error);
        }
    });
}else{
    var mesactual = $('select[name=mes]').val()
}


$.ajax({
    url: url2,
    method: 'GET',
    success: function(response){
            pintar3(response)
    },
    error: function(error){
        console.log(error);
    }
});

$.ajax({
    url: url3+"/"+mesactual,
    method: 'GET',
    success: function(response){
            console.log(response)
            pintar5(response)
    },
    error: function(error){
        console.log(error);
    }
});

$.ajax({
    url: url4+"/"+mesactual,
    method: 'GET',
    success: function(response){

    pintar4(response)
    },
    error: function(error){
        console.log(error);
    }
});

function pintar(data){
          var doughnutData = [{
                 value: parseInt(data.auditadas),
                 color: "#1ABC9C",
                 highlight: "#1ABC9C",
                 label: "Auditadas"
             }, {
                 value:parseInt(data.programadas),
                 color: "#556B8D",
                 highlight: "#556B8D",
                 label: "Por Auditar"
             }
         ];
         var ctx3 = document.getElementById("doughnut").getContext("2d");
         window.myDoughnut = new Chart(ctx3).Doughnut(doughnutData, {
             responsive: true
         });

}

function pintar5(data){
    console.log(data)
    var doughnutData = [{
           value: parseInt(data.auditadas),
           color: "#1ABC9C",
           highlight: "#1ABC9C",
           label: "Auditadas"
       }, {
           value:parseInt(data.programadas),
           color: "#556B8D",
           highlight: "#556B8D",
           label: "Por Auditar"
       }
   ];
   var ctx3 = document.getElementById("doughnut1").getContext("2d");
   window.myDoughnut = new Chart(ctx3).Doughnut(doughnutData, {
       responsive: true
   });

}

function pintar4(data){
    var doughnutData = [{
           value: parseInt(data.auditadas),
           color: "#1ABC9C",
           highlight: "#1ABC9C",
           label: "Auditadas"
       }, {
           value:parseInt(data.programadas),
           color: "#556B8D",
           highlight: "#556B8D",
           label: "Por Auditar"
       }
   ];
   var ctx3 = document.getElementById("doughnut2").getContext("2d");
   window.myDoughnut = new Chart(ctx3).Doughnut(doughnutData, {
       responsive: true
   });

}


function pintar2(data){


    var doughnutData = [{
        value: parseInt(data.auditadas),
        color: "#1ABC9C",
        highlight: "#1ABC9C",
        label: "Auditadas"
    }, {
        value:parseInt(data.programadas),
        color: "#556B8D",
        highlight: "#556B8D",
        label: "Por Auditar"
    }
    ];
    var ctx3 = document.getElementById("doughnut").getContext("2d");
    window.myDoughnut = new Chart(ctx3).Doughnut(doughnutData, {
        responsive: true
    });



    
    
    var randomScalingFactor = function() {
        return Math.round(Math.random() * 100)
    };

    let label = []
    let datalist = []
    let novedad = []
    for (i=0;i<data.length;i++) {
        label.push(data[i].name);
        datalist.push(data[i].total);
        novedad.push(data[i].novedad);
     }

     var randomScalingFactor = function() {
        return Math.round(Math.random() * 100)
    };
    var barChartData = {
        labels:label,
        

   
        datasets: [{
            label: 'CPU Load',
            fillColor: 'rgba(26,188,156,0.5)',
            strokeColor: 'rgba(255,255,255,0.8)',
            highlightFill: 'rgba(26,188,156,1)',
            highlightStroke: 'rgba(255,255,255,0.8)',
            data: datalist
        }, {
            label: 'CPU 222',
            fillColor: 'rgba(31,123,182,0.5)',
            strokeColor: 'rgba(255,255,255,0.8)',
            highlightFill: 'rgba(31,123,182,1)',
            highlightStroke: 'rgba(255,255,255,0.8)',
            data: datalist
        }]

    }
        var ctx2 = document.getElementById("bar").getContext("2d");
   
        window.myBar = new Chart(ctx2).Bar(barChartData, {
            responsive: true
            
        });
    



}

function pintar3(data){

    console.log(data);

    let datalist = ['datalist']
    let novedad1 = ['FUERA DEL AIRE']
    let novedad2 = ['APAGADA']
    let novedad3 = ['COMPETENCIA']
    let novedad4 = ['<> MARCAS CLIENTE']
    let novedad5 = ['REP CUÑA']
    let novedad6 = ['MARCA <> REF']
    for (i=0;i<data.length;i++) {
        datalist.push(data[i].name);
        novedad1.push(data[i].novedad1);
        novedad2.push(data[i].novedad2);
        novedad3.push(data[i].novedad3);
        novedad4.push(data[i].novedad4);
        novedad5.push(data[i].novedad5);
        novedad6.push(data[i].novedad6);
     }
    var chart = c3.generate({
        bindto: '#combination-chart',
        data: {
            x: 'datalist',
            xFormat: '%Y',
           columns: [
                datalist,
                novedad1,
                novedad2,
                novedad3,
                novedad4,
                novedad5,
                novedad6,
           ],
           type: 'bar',


       },
       axis: {
           x: {
               type: 'categorized'
           }
       }
   }); 
};
 
function tooltip_contents(d, defaultTitleFormat, defaultValueFormat, color) {
    var $$ = this, config = $$.config, CLASS = $$.CLASS,
        titleFormat = config.tooltip_format_title || defaultTitleFormat,
        nameFormat = config.tooltip_format_name || function (name) { return name; },
        valueFormat = config.tooltip_format_value || defaultValueFormat,
        text, i, title, value, name, bgcolor;
    
    // You can access all of data like this:
    //console.log($$.data.targets);
    console.log(d);
    
    for (i = 0; i < d.length; i++) {
        if (! (d[i] && (d[i].value || d[i].value === 0))) { continue; }

        // to exclude
        //if (d[i].name === 'data2') { continue; }
        
        if (! text) {
            title =  d[0].x.getFullYear();
            text = "<table class='" + CLASS.tooltip + "'>" + (title || title === 0 ? "<tr><th colspan='2'>" + title + "</th></tr>" : "");
        }

        name = nameFormat(d[i].name);
        value = valueFormat(d[i].value, d[i].ratio, d[i].id, d[i].index);
        bgcolor = $$.levelColor ? $$.levelColor(d[i].value) : color(d[i].id);

        text += "<tr class='" + CLASS.tooltipName + "-" + d[i].id + "'>";
        text += "<td class='name'><span style='background-color:" + bgcolor + "'></span>" + name + "</td>";
        text += "<td class='value'>" + value + "</td>";
        text += "</tr>";
    }
    return text + "</table>";
}

