
Dropzone.options.createProject = {
    url: 'enviar2.php',
    autoProcessQueue: true,
    parallelUploads: 11,
    uploadMultiple: true,
    maxFiles: 10,
    previewsContainer: ".dropzone",
    dictDefaultMessage: 'Cargar Imagenes aquí',
    clickable: '.dropzone',
    resizeWidth: 720,
    resizeHeight: null,
    resizeMimeType: null,
    resizeQuality: 0.8,
    resizeMethod: 'contain',
    filesizeBase: 1024,
    paramName:"files",
    params: {  },
    init: function() {
        myDropzone=this;
        document.getElementById("enviardatos").addEventListener("click", function(e){
            if($('#volver').val()==3){
                myDropzone.options.resizeWidth = null;
            }else{
                myDropzone.options.resizeWidth = 720;
            }
            e.preventDefault();
            e.stopPropagation();           
            myDropzone.on("sending", function(file, xhr, formData) {
            var datastring = $("#auditoria").serializeArray();
            var total = datastring.length;
            for (var i=0; i<total; i++) { 
                formData.append(datastring[i]['name'],datastring[i]['value'] );  
            }   
            });
            myDropzone.processQueue();
            document.body.scrollTop = 0;
            document.getElementById('loading_oculto').style.visibility='visible';
            document.getElementById('mainwrapper').style.visibility='hidden';
        });
        this.on('addedfile', function(file){
         //   $('.upload-box').hide();
        });
        this.on('success', function(file){
            let semana =  $("#semana").val()
            let codigo =  $("#codigo").val()
            document.getElementById('loading_oculto').style.visibility='hidden';
            document.getElementById('mainwrapper').style.visibility='visible';

            switch(file.xhr.response) {
                case 1:
                    window.location='llenardatosprueba.php?ws_Msj=0&enviado=1&semana='+semana +'&codigo='+codigo
                    break;
                case 2:
                    window.location='llenardatosprueba.php?ws_Msj=0&enviado=1&semana='+semana +'&codigo='+codigo
                    break;

                case 3:
                    window.location='llenardatosprueba.php?ws_Msj=0&enviado=1&semana='+semana +'&codigo='+codigo
                    break;
                default:
                    window.location='llenardatosprueba.php?ws_Msj=0&enviado=1&semana='+semana +'&codigo='+codigo
            }

            ;

        });
    }
}
