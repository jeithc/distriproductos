
$(function(){
  switch ($("#result").val()) {
    case "clientRepeat":
      showErrorMessage("Error, Cliente duplicado")
      break;
    case "clientOk":
      showSuccess("Cliente Creado")
      break;
    case "policyOk":
      showSuccess("Polizá agregada con éxito")
      break;
    case "CustomerDelete":
      showSuccess("Cliente Eliminado con éxito")
      break;
    case "clientEdit":
    showSuccess("Cliente Editado con éxito")
      
    default:
      return false
      
  }

  
});

function showSuccess(msg){
  crear()
  Messenger().post({
   message: msg,
  type: 'success',
    showCloseButton: true
  });
}

function showErrorMessage(msg){
  crear()
 Messenger().post({
	 message: msg,
	type: 'error',
    showCloseButton: false
	});
}	
function crear(){
  var loc = ['top', 'center'];
  var style = 'flat';

  var $output = $('.controls output');
  var $lsel = $('.location-selector');
  var $tsel = $('.theme-selector');

  var update = function(){
    var classes = 'messenger-fixed';

    for (var i=0; i < loc.length; i++)
      classes += ' messenger-on-' + loc[i];

    $.globalMessenger({ extraClasses: classes, theme: style });
    Messenger.options = { extraClasses: classes, theme: style };

    $output.text("Messenger.options = {\n    extraClasses: '" + classes + "',\n    theme: '" + style + "'\n}");
  };

  update();
}
