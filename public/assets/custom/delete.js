var home = document.location.href;


$(document).ready(function() {
    $('#example').dataTable();
});

function delete_product(id){
    event.preventDefault(); 
    swal({
        title: "¿estás seguro?",
        text: "al eliminar, no se podrá usar mas el producto",
        icon: "warning",
        dangerMode: true,
        buttons: ["Cancelar", "Eliminar"],
    })
    .then((willDelete) => {
        console.log(willDelete)
    if (willDelete) {
        window.location.href = `${home}/delete/${id}`;
    } else {
        return false
    }
    });


}

function delete_category(id){
    event.preventDefault(); 
    swal({
        title: "¿estás seguro?",
        text: "al eliminar, no se podrá usar mas la categoría",
        icon: "warning",
        dangerMode: true,
        buttons: ["Cancelar", "Eliminar"],
    })
    .then((willDelete) => {
        console.log(willDelete)
    if (willDelete) {
        window.location.href = `${home}/delete/${id}`;
    } else {
        return false
    }
    });


}








