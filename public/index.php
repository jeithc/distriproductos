<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
date_default_timezone_set('America/Bogota');
setlocale(LC_ALL,"es_ES");
$hoy = date('Y-m-d');
$ahora = date('Y-m-d H:i:s');
error_reporting(E_ALL);
set_time_limit(300);
//php route
session_start();
require_once '../vendor/autoload.php';
require_once '../config/database_config.php';
require_once '../config/define.php';

$route = $_GET['route'] ?? '/';

function render($fileName, $params = []){
    ob_start();
    extract($params);
    include $fileName;
    return ob_get_clean();
}

use Phroute\Phroute\RouteCollector;
$router = new RouteCollector();


$router->controller('/', app\controllers\OrderController::class);
$router->controller('/confirm', app\controllers\ConfirmController::class);
$router->controller('/orders', app\controllers\ListController::class);
$router->controller('/error', app\controllers\ErrorController::class);
$router->controller('/product', app\controllers\ProductController::class);
$router->controller('/category', app\controllers\CategoryController::class);
$router->controller('/rest', app\controllers\RestController::class);



$dispatcher = new Phroute\Phroute\Dispatcher($router->getData());
    $response = $dispatcher->dispatch($_SERVER['REQUEST_METHOD'], $route);
try {
    
} catch (\Exception $e) {
    switch (get_class($e)) {
        case 'Phroute\\Phroute\\Exception\\HttpRouteNotFoundException':

        header('Location:' . BASE_URL. 'error/404' );
            break;
        case 'Phroute\\Phroute\\Exception\\HttpMethodNotAllowedException':

        header('ERROR 500:' . BASE_URL. 'error/500' );
            break;
        default:

        header('Location:' . BASE_URL. 'error/500' );        
            break;
    }
}
echo $response;