/*
SQLyog Ultimate v12.09 (64 bit)
MySQL - 10.4.11-MariaDB : Database - orders_company
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`orders_company` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish_ci */;

USE `orders_company`;

/*Table structure for table `category` */

DROP TABLE IF EXISTS `category`;

CREATE TABLE `category` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `description` text COLLATE utf8_spanish_ci NOT NULL,
  `active` tinyint(1) DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

/*Data for the table `category` */

insert  into `category`(`id`,`name`,`description`,`active`) values (1,'cat11','descripcion de categoria 1',1),(2,'cat2','descripcion de categoria 2',0),(5,'cat4','desc cat 4',0),(6,'cat2','descripcion cat 2',1);

/*Table structure for table `order` */

DROP TABLE IF EXISTS `order`;

CREATE TABLE `order` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `client_id_type` enum('CC','NIT','CE') COLLATE utf8_spanish_ci NOT NULL,
  `client_id` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `client_name` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `client_address` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `client_city` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `client_email` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

/*Data for the table `order` */

insert  into `order`(`id`,`client_id_type`,`client_id`,`client_name`,`client_address`,`client_city`,`client_email`,`date`) values (14,'CC','1140814372','Jeith Carrillo','Calle 95 # 71-31 34','Bogota','jeith2@gmail.com','2020-04-29 18:31:34'),(15,'NIT','564745674567','Milena','Calle 95 # 71-31','Bogota','jeith2@gmail.com','2020-04-29 18:32:48'),(16,'CC','342345234','Carlos','Calle 95 # 71-31','Miami','jeith2@gmail.com','2020-04-30 18:48:07');

/*Table structure for table `order_products` */

DROP TABLE IF EXISTS `order_products`;

CREATE TABLE `order_products` (
  `id_products` int(11) unsigned NOT NULL,
  `id_order` int(11) unsigned NOT NULL,
  `amount` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_products`,`id_order`),
  KEY `id_order` (`id_order`),
  CONSTRAINT `order_products_ibfk_1` FOREIGN KEY (`id_products`) REFERENCES `products` (`id`),
  CONSTRAINT `order_products_ibfk_2` FOREIGN KEY (`id_order`) REFERENCES `order` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

/*Data for the table `order_products` */

insert  into `order_products`(`id_products`,`id_order`,`amount`) values (1,14,1),(1,16,3),(2,14,1),(2,15,2),(2,16,3),(3,14,1),(3,15,2),(7,16,3);

/*Table structure for table `products` */

DROP TABLE IF EXISTS `products`;

CREATE TABLE `products` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `description` text COLLATE utf8_spanish_ci NOT NULL,
  `bar_code` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `price` float NOT NULL,
  `available` int(100) NOT NULL,
  `id_category` int(11) unsigned NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1 COMMENT 'producto activo',
  PRIMARY KEY (`id`),
  KEY `id_category` (`id_category`),
  CONSTRAINT `products_ibfk_1` FOREIGN KEY (`id_category`) REFERENCES `category` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

/*Data for the table `products` */

insert  into `products`(`id`,`name`,`description`,`bar_code`,`price`,`available`,`id_category`,`active`) values (1,'producto11','descripcion de producto 1','12323543452',100,7,1,1),(2,'producto2','descripcion del producto 2','123459645',50,0,1,1),(3,'producto3','descripcion del producto 3','123459646',55,4,1,0),(4,'producto4','descripcion del producto 4','123459647',56,5,1,0),(6,'producto5','descripcion del producto 5','123459648',40,5,2,0),(7,'prducto8','desc prod 8','15618965289',200,37,6,1);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
