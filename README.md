# PROYECTO DE PRUEBA TÉCNICA - BACKEND DEVELOPER
**Educonnecting**

 - Lenguaje Utilizado: PHP* con dependencias composer, javascript y Jquery
 - librerías : bootstrap, twig (para salida html)

*Se utilizo este lenguaje por su rapidez a la hora de hacer conexiones con BD y mostrar resultados de la misma ademas de la sencillez al momento de montar la prueba en algún servidor para revisarla*

> NOTA: el archivo .ini del servidor php debe tener activo
> "extension=intl" esto para poder manejar las rutas de PHPRoute

# Servicios REST

 - /rest/orders    
	 - muestra todas las ordenes creadas
 - rest/ordersnum/[#id_order]
	 - muestra los datos de una orden en especifico
 - rest/category/
	 - muestra todas las categorías
 - rest/products/
	 - muestra todos los artículos creados